const users = [
    {
        id: 1,
        name: 'Jair Tasayco'
    },
    {
        id: 2,
        name: 'Antonio Tasayco'
    },
    {
        id: 3,
        name: 'Jair Bautista.'
    },
    {
        id: 4,
        name: 'Nuevo item.'
    }
  ];

var express = require('express')
var http = require('http')
var app = express()

app.get('/', (req, res) => {
  res.status(200).send(users)
})

http.createServer(app).listen(4000, () => {
  console.log('Server started at http://localhost:4000');
});